class Cuenta:
	def __init__(self, nom, num, sal = 0):
		self.titular = nom
		self.numero = num
		self.saldo = sal
		
	def ingreso(self, cantidad):
		self.saldo = self.saldo + cantidad
		
	def reintegro(self, cantidad):
		self.saldo = self.saldo - cantidad
	
	def transferenciaM(self, dst, cantidad):
		self.reintegro(cantidad) #para hacer transeferencia te tineens que quitar el dinero
		dst.ingreso(cantidad) #ingreso al destinatario una cantidad
		
	def transferenciaF(org, dst, cantidad):
		self.reintegro(cantidad)
		dst.ingreso(cantidad) 
		
	def __str__(self):
		return "#{num} con saldo:{sal}".format(num=self.numero, sal= self.saldo)
		
cc1 = Cuenta("Pepe", 1)
cc2 = Cuenta("Ana", 2, 5000)

cc1 .ingreso(3000)
print(cc1)

cc1.ingreso(1000)
print(cc1)

cc2.ingreso(3000) #ingreso a cc2 3000euros
print(cc2)
	
cc2.transferenciaM(cc1, 1500) #un metodo para hacer transferencia de cc1 a cc2 de 1500 euros
Cuenta.transferenciaF(cc1, cc2, 1500) #una funcion que hace transferencias, le dices el origen el destinatario y la cantidad
print(cc1)
print(cc2)
